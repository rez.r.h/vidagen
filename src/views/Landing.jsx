import React, { Component } from "react";
import ProductItemMedium from "../components/ProductItemMedium";
import ProductContainerMedium from "../components/ProductContainerMedium";
import Banner from "../components/Banner";
import PatentContainer from "../components/PatentContainer";
import Medals from "../components/Medals";
import AboutSummary from "../components/AboutSummary";
import ContactUsSummary from "../components/ContactUsSummary";
import "../styles/landing.scss";
import Veterinary from "../assets/veterinary.png";
import Doctor from "../assets/doctor.png";
import Tasis from "../assets/parvane_1.jpg";
import Sanat from "../assets/parvane_2.jpg";
import Parvane from "../assets/parvane_3.jpg";

class Landing extends Component {
  render() {
    return (
      <div>
        <Banner></Banner>
        <AboutSummary></AboutSummary>

        <h3>محصولات ویداژن</h3>
        <div id="category_container">
          <div className="category_item">
            <div
              onClick={() => {
                window.location = window.origin + "/products/dr";
              }}
            >
              <img src={Doctor}></img>
            </div>
            <span>محصولات پزشکی</span>
          </div>
          <div className="category_item">
            <div
              onClick={() => {
                window.location = window.origin + "/products/veterinary";
              }}
            >
              <img src={Veterinary}></img>
            </div>
            <span>محصولات دام پزشکی</span>
          </div>
        </div>
        <h3>افتخارات و گواهی نامه ها</h3>
        <div
          style={{
            display: "inline-flex",
            margin: "48px 24px",
            justifyContent: "center",
            width: "100%",
            background: "#efefef",
          }}
        >
          <Medals src={Tasis}></Medals>
          <Medals src={Sanat}></Medals>
          <Medals src={Parvane}></Medals>
        </div>
        {/* <ProductContainerMedium>
          <ProductItemMedium></ProductItemMedium>
          <ProductItemMedium></ProductItemMedium>
          <ProductItemMedium></ProductItemMedium>
          <ProductItemMedium></ProductItemMedium>
        </ProductContainerMedium> */}
        <h3>مقالات</h3>
        <PatentContainer></PatentContainer>
        <ContactUsSummary></ContactUsSummary>
      </div>
    );
  }
}

export default Landing;
