import React, { Component } from "react";
import "../styles/contact_us.scss";

class ContactUs extends Component {
  render() {
    return (
      <div id="contact_us">
        <header>
          ارتباط با ما
        </header>
        <table>
          <tr>
            <td> آدرس کارخانه</td>
            <td>
              استان گلستان کيلومتر 7 جاده گرگان کردکوي، بعد از پمپ بنزين کوثر،
              پلاک184
            </td>
          </tr>
          <tr>
            <td> آدرس دفتر مرکزی:</td>
            <td>
              تهران، يوسف آباد، خيابان شهيد جهان آرا، خيابان چهلم، پلاک 30، طبقه
              همکف
            </td>
          </tr>
        </table>
        <span>اطلاعات تماس</span>
        <table>
          <tr>
            <td>تلفن کارخانه</td>
            <td>۰۱۷۳۲۶۱۶۳۰۰</td>
          </tr>
          <tr>
            <td>تلفن دفتر مرکزی</td>
            <td>۰۲۱۸۸۰۳۳۳۰۲</td>
          </tr>
          <tr>
            <td>فکس</td>
            <td>۰۲۱۸۸۰۳۳۳۰۲</td>
          </tr>
          <tr>
            <td>ایمیل روابط عمومی</td>
            <td>info@vgpharmed.com</td>
          </tr>
        </table>
      </div>
    );
  }
}

export default ContactUs;
