import React, { Component } from "react";
import "../styles/about_us.scss";
import Company from "../assets/company.jpg";
import CompanyValues from "../components/CompanyValues";

class AboutUs extends Component {
  render() {
    return (
      <div id="about_us">
        <img alt="company" src={Company}></img>
        <header>شرکت داروسازی ویداژن فارمد</header>
        <p>
          شرکت سهامی خاص ویدا ژن فارمد در سال 1397 با هدف تولید انواع ضد عفوني
          كننده هاي دام و طيور و همچنين توليد داروهاي دام و طيور با منشاء گياهي
          به صورت محلول و شربت خوراكي ثبت شده است. از انواع محصولات ضدعفونی این
          شرکت می توان به ضد عفونی کننده های سطوح کنسانتره و سطوح آماده به مصرف
          اشاره نمود.
        </p>
        <CompanyValues></CompanyValues>
      </div>
    );
  }
}

export default AboutUs;
