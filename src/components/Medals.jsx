import React, { Component } from "react";

class Medals extends Component {
  render() {
    return (
      <div
        style={{
          width: "200px",
          margin: "48px",
          padding: "2px",
          background: "#afafaf",
          display: "inline-flex",
        }}
      >
        <img
          alt=""
          style={{ width: "100%", height: "auto" }}
          src={this.props.src}
        ></img>
      </div>
    );
  }
}

export default Medals;
