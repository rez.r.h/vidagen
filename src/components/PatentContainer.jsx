import React, { Component } from "react";
import Patent1 from "../assets/patent_1.png";
import Patent2 from "../assets/patent_2.png";
import Patent3 from "../assets/patent_3.jpg";
import Patent4 from "../assets/patent_4.jpg";
import Patent5 from "../assets/patent_5.jpg";
import Patent6 from "../assets/patent_6.jpg";
import Patent7 from "../assets/patent_7.jpg";
import PatentItem from "../components/PatentItem";
import "../styles/patent_container.scss";
import Utils from "../utils/Utils";
import book from "../assets/patents/book.pdf";
import sample from "../assets/patents/sample.pdf";
import patent1 from "../assets/patents/patent_1.pdf";
import patent2 from "../assets/patents/patent_2.pdf";
import patent3 from "../assets/patents/patent_3.pdf";
import patent4 from "../assets/patents/patent_4.pdf";
import patent5 from "../assets/patents/patent_5.pdf";

class PatentContainer extends Component {
  constructor(props) {
    super(props);
    this.utils = new Utils();
  }

  render() {
    return (
      <div
        className="patent_container"
        style={{
          display: "grid",
          flexFlow: "row",
          background: "#efefef",
        }}
      >
        <PatentItem
          src={Patent1}
          onClick={() => {
            this.utils.download("Organic_Chemistry.pdf", sample);
          }}
        >
          name
        </PatentItem>
        <PatentItem
          src={Patent2}
          onClick={() => {
            this.utils.download(
              "Sample_preparation_for_liquid_chromatography.pdf",
              book
            );
          }}
        >
          name
        </PatentItem>
        <PatentItem
          src={Patent3}
          onClick={() => {
            this.utils.download("پتنت در مورد ضد عفونی کننده.pdf", patent1);
          }}
        >
          name
        </PatentItem>
        <PatentItem
          src={Patent4}
          onClick={() => {
            this.utils.download("مقاله در مورد ضد عفونی کننده.pdf", patent2);
          }}
        >
          name
        </PatentItem>
        <PatentItem
          src={Patent5}
          onClick={() => {
            this.utils.download("مقاله در مورد ضد عفونی کننده.pdf", patent3);
          }}
        >
          name
        </PatentItem>
        <PatentItem
          src={Patent6}
          onClick={() => {
            this.utils.download("مقاله در مورد ضد عفونی.pdf", patent4);
          }}
        >
          name
        </PatentItem>
        <PatentItem
          src={Patent7}
          onClick={() => {
            this.utils.download("مقاله در مورد ضد عفونی.pdf", patent5);
          }}
        >
          name
        </PatentItem>
      </div>
    );
  }
}

export default PatentContainer;
