import React, { Component } from "react";
import "../styles/product_container-medium.scss";

class ProductContainerMedium extends Component {
  render() {
    return (
      <div className="product_container-medium">{this.props.children}</div>
    );
  }
}

export default ProductContainerMedium;
