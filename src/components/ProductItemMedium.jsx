import React, { Component } from "react";
import "../styles/product_item-medium.scss";

class ProductItemMedium extends Component {
  render() {
    return (
      <div className="product_item-medium">
        <img />
        <span>
          ضدعفونی کننده دست<strong>Prospet H</strong>
        </span>
        <span>۱۰۰ سی سی</span>
        <span> ۱۰۰,۰۰۰ تومان</span>
        <span></span>
      </div>
    );
  }
}

export default ProductItemMedium;
