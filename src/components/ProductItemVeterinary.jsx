import React, { Component } from "react";
import "../styles/product_item.scss";

class ProdoctItemVeterinary extends Component {
  render() {
    return (
      <div className="product_item">
        <img></img>
        <div className="product_item_text_container">
          <strong>نام محصول: </strong>
          <span>{this.props.info.name}</span>
          <br></br> <br></br>
          <strong>کارایی: </strong>
          <span>{this.props.info.usage}</span>
          <br></br> <br></br>
          <strong>حجم: </strong>
          <span>{this.props.info.val}</span>
          <br></br> <br></br>
          <strong>ترکیبات: </strong>
          <span>{this.props.info.material}</span>
          <br></br> <br></br>
          <strong>شرایط نگه داری: </strong>
          <span>{this.props.info.method}</span>
        </div>
      </div>
    );
  }
}

export default ProdoctItemVeterinary;
