import React, { Component } from "react";
import "../styles/header_item.scss";

class HeaderItem extends Component {
  render() {
    let { ...atr } = this.props;
    return (
      <div className="header_item" {...atr}>
        <img src={this.props.src} />
        <span>{this.props.children}</span>
      </div>
    );
  }
}

export default HeaderItem;
