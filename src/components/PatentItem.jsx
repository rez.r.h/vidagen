import React, { Component } from "react";
import "../styles/patent_item.scss";

class ProductItemMedium extends Component {
  render() {
    let { ...attr } = this.props;
    return (
      <div className="patent_item" {...attr}>
        <img alt="" src={this.props.src} />
        <span>{/* <strong>{this.props.children}</strong> */}</span>
      </div>
    );
  }
}

export default ProductItemMedium;
