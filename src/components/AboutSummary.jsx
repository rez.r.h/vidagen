import React from "react";
import "../styles/about_summary.scss";

class AboutSummary extends React.Component {
  render() {
    return (
      <div id="about_summary">
        <span>شرکت داروسازی ویداژن فارمد</span>
        <p>
          شرکت سهامی خاص ویدا ژن فارمد در سال 1397 با هدف تولید انواع ضد عفوني
          كننده هاي دام و طيور و همچنين توليد داروهاي دام و طيور با منشاء گياهي
          به صورت محلول و شربت خوراكي ثبت شده است. از انواع محصولات ضدعفونی این
          شرکت می توان به ضد عفونی کننده های سطوح کنسانتره و سطوح آماده به مصرف
          اشاره نمود.
        </p>
      </div>
    );
  }
}

export default AboutSummary;
