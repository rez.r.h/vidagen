import React from "react";
import "../styles/footer.scss";

class Footer extends React.Component {
  render() {
    return (
      <div id="footer">
        <span
          className="footer_item"
          onClick={() => {
            window.location = window.origin + "/contact";
          }}
        >
          ارتباط با ویداژن
        </span>
        <span
          className="footer_item"
          onClick={() => {
            window.location = window.origin + "/about";
          }}
        >
          درباره ی ویداژن
        </span>
        <span
          className="footer_item"
          onClick={() => {
            window.location = window.origin;
          }}
        >
          محصولات ویداژن
        </span>
        <span className="footer-end">
          آدرس کارخانه: استان گلستان کيلومتر 7 جاده گرگان کردکوي، بعد از پمپ
          بنزين کوثر
        </span>
      </div>
    );
  }
}

export default Footer;
