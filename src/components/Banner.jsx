import React, { Component } from "react";
import "../styles/banner.scss";
import ProductImage from "../assets/company_pic1.jpg";

class Banner extends Component {
  render() {
    return (
      <div className="banner">
        <img src={ProductImage}></img>
      </div>
    );
  }
}

export default Banner;
