import React from "react";
import "../styles/contact_us_summary.scss";
import CallIcon from "../assets/phone.png";
import EmailIcon from "../assets/email.png";

class ContactUsSummary extends React.Component {
  render() {
    return (
      <div id="contact_us_summary">
        <span>پشتیبانی</span>
        <img src={CallIcon} />
        <span>شماره تماس:</span>
        <span className="contact_us_summary_link">۰۱۷۳۲۶۱۶۵۴۳</span>
        <img src={EmailIcon} />
        <span>پست الکترونیک:</span>
        <span className="contact_us_summary_link"> info@vgpharmed.com </span>
      </div>
    );
  }
}

export default ContactUsSummary;
