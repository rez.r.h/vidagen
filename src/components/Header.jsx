import React, { Component } from "react";
import "../styles/header.scss";
import Contact from "../assets/contact.png";
import Vidagen from "../assets/vidagen.png";
import HeaderItem from "../components/HeaderItem";
import { Link } from "react-router-dom";
class Header extends Component {
  render() {
    return (
      <div id="header">
        <img id="header_logo" alt="logo" src={Vidagen} />
        {/* <HeaderItem
          src={Contact}
          onClick={() => {
            window.location = window.origin + "/contact";
          }}
        >
          hello
        </HeaderItem> */}
        <span
          onClick={() => {
            window.location = window.origin + "/contact";
          }}
        >
          ارتباط با ویداژن
        </span>
        <span
          onClick={() => {
            window.location = window.origin + "/about";
          }}
        >
          درباره ی ویداژن
        </span>
        <span
          onClick={() => {
            window.location = window.origin;
          }}
        >
          محصولات ویداژن
        </span>
        <span
          onClick={() => {
            window.location = window.origin + "/patents";
          }}
        >
          مقالات و کتب
        </span>
      </div>
    );
  }
}

export default Header;
