import React, { Component } from "react";
import "../styles/company_values.scss";

class CompanyValues extends Component {
  render() {
    return (
      <div id="company_values">
        <div id="company_values-items">
          <span>چشم انداز</span>
          <ul>
            <li>
              تبدیل شدن به یک شرکت معتبر جهانی در زمینه دانش و تولید ایزوتوپ های
              پایدار
            </li>
          </ul>
        </div>
        <div id="company_values-items">
          <span>ارزش های بنیادی</span>
          <ul>
            <li>تعالی فردی و سازمانی</li>
            <li>خودباوری و دانش محوری</li>
            <li>حفظ کرامت انسانی</li>
            <li>پیشکام و پیشرو بودن</li>
            <li>توجه به توسعه پایدار و حفظ منابع</li>
          </ul>
        </div>
        <div id="company_values-items">
          <span>هدف بنیادی</span>
          <ul>
            <li>گسترش صلح و رفاه با نوآوری در دانش و فن آوری</li>
          </ul>
        </div>
      </div>
    );
  }
}

export default CompanyValues;
