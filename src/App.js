// import logo from "./logo.svg";
import "./App.css";
import AboutSummary from "./components/AboutSummary";
import ContactUsSummary from "./components/ContactUsSummary";
import Footer from "./components/Footer";
import AboutUs from "./views/AboutUs";
import ContactUs from "./views/ContactUs";
import Pattents from "./views/Pattents";
import Product from "./views/Product";
import Banner from "./components/Banner";
import HeaderItem from "./components/HeaderItem";
import Landing from "./views/Landing";
import Header from "./components/Header";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

function App() {
  document.title = "ویداژن";
  return (
    <Router>
      <div
        style={{
          minHeight: "100%",
          display: "flex",
          flexFlow: "column",
          // justifyContent: "center",
        }}
      >
        <Header></Header>
        <div id="header_gap"></div>
        <div>
          <Switch>
            <Route path="/about">
              <AboutUs></AboutUs>
            </Route>
            <Route path="/products">
              <Product></Product>
            </Route>
            <Route path="/patents">
              <Pattents></Pattents>
            </Route>
            <Route path="/contact">
              <ContactUs></ContactUs>
            </Route>
            <Route path="/">
              <Landing></Landing>
            </Route>
          </Switch>
        </div>

        {/* <AboutSummary></AboutSummary> */}
        {/* <ContactUsSummary></ContactUsSummary> */}
        <Footer></Footer>
      </div>
    </Router>

    // <div className="App">
    //   <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //     <p>
    //       Edit <code>src/App.js</code> and save to reload.
    //     </p>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       Learn React
    //     </a>
    //   </header>
    // </div>
  );
}

export default App;
